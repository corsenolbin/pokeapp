package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import PokemonClient.PokemonClient;
import contracts.Pokemon.Pokemon;
import src.PokemonSettings;
import store.PokemonFileService;

public class PokemonController {
	
	public static void GetAndSaveSpritePics(Pokemon pokemon) {

		HashMap<String,String> spriteMap = pokemon.getSprites();
		
		spriteMap.forEach((key, url) -> {
			if(url != null) {
				BufferedImage image = PokemonClient.GetImageByURL(url);
				File file = PokemonFileService.createImageFile(pokemon, key);
				PokemonFileService.WriteImage(image, file);	
			}			
		});		
	}
	
	public static Pokemon getPokemonByName(String name) {
		Pokemon poke = PokemonFileService.LoadJSONObject(name);
		
		if(poke != null) {
			return poke;
		}
		
		poke = PokemonClient.GetPokemonByName(name);

        if(poke != null)
            PokemonFileService.SaveJSONObject(poke);
        
		return poke;
	}
	
	public static Pokemon getPokemonById(int id) {
        PokemonSettings settings = PokemonSettings.getInstance();
        
        Pokemon poke = null;
		
        if(!settings.pokeHashContainsValue(id))
            poke = PokemonClient.GetPokemonById(id);
        
		if(poke != null)
			PokemonFileService.SaveJSONObject(poke);
		
		return poke;
	}
	
	public static List<String> getAllPokemonNamesFromFiles() {
		List<String> pokemons = new ArrayList<String>();
		
		pokemons.addAll(PokemonFileService.getAllPokemonNames());
		
		return pokemons;
	}
	
	public static InputStream getPokemonFrontDefaultPicture(Pokemon poke) {
		
		InputStream picture = PokemonFileService.getPictureStream(poke.getName(), "frontDefault");
		
		if(picture == null) {
			
			try {
				BufferedImage picFromURL = PokemonClient.GetImageByURL(poke.getSprites().get("frontDefault"));
				File pictureFile = PokemonFileService.createImageFile(poke, "frontDefault");
				PokemonFileService.WriteImage(picFromURL, pictureFile);
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				ImageIO.write(picFromURL, "png", output);
				picture = new ByteArrayInputStream(output.toByteArray());
			} catch (Exception e) {
				e.printStackTrace();
			}		
		}
		
		return picture;	
	}
	
	public static <T> T getObjectfromURL(String url, Class<T> genClass) {
		return PokemonClient.GetObjectByURL(url, genClass);
	}
}
