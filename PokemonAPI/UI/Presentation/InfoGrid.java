package Presentation;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class InfoGrid extends GridPane{
    public InfoGrid() {
        ColumnConstraints column0 = new ColumnConstraints();
        column0.setHalignment(HPos.RIGHT);
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.LEFT);
        
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setHgrow(Priority.ALWAYS);
        
        getColumnConstraints().addAll(column0, column1, column2);    
        
        setPadding(new Insets(5));
        
        setHgap(5);
        setVgap(1);
        
    }
}