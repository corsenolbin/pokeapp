package Presentation;

import src.PokemonSettings;

import java.util.List;

import contracts.Pokemon.Pokemon;
import controllers.PokemonController;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import store.PokemonFileService;

public class MasterView extends Application{
	
	private FlowPane notesFlow;
	private FlowPane menuFlow;
	private ScrollPane sideFlow;
	final BorderPane rootNode = new BorderPane();
	
	public void init() {
		
	}
	
	public void start(Stage myStage) {
		
		Scene myScene = new Scene(rootNode, 800, 600);		
		myStage.setScene(myScene);
		myStage.setTitle("Notes App");
		
		
		notesFlow = new FlowPane(5, 5);
		notesFlow.setStyle("-fx-padding: 5;"
				+ "-fx-background-color: white;");
		
		setSideFlow(new SideBar(rootNode));
		setupMenu();
		rootNode.setTop(menuFlow);
		
		//rootNode.setCenter(notesFlow);
		
		rootNode.setLeft(sideFlow);
		
		myStage.setOnCloseRequest(ae -> {
			PokemonSettings settings = PokemonSettings.getInstance();
			
			List<String> names = PokemonFileService.getAllPokemonNames();
	        
	        for(String name: names) {
	            Pokemon poke = PokemonController.getPokemonByName(name);
	            settings.addToPokemonSaved(poke.getName(), poke.getId());
	        }
			PokemonFileService.SaveJSONSettings(settings);
			
	        
		});
		
		myStage.show();
			
	}
	
	void setupMenu() {
		menuFlow = new FlowPane(10, 10);
		
		menuFlow.setStyle("-fx-padding: 5; "
							+ "-fx-background-color: red;"
							+ "-fx-border-width: 0 0 3 0;"
							+ "-fx-border-color: black;");

		menuFlow.setMinHeight(100);
		menuFlow.setAlignment(Pos.TOP_RIGHT);

		Button bSettings = new Button("Settings");
		
		bSettings.setOnMouseClicked((ae) -> {
			SettingsView sView = new SettingsView();
			sView.start();
		});
		
		menuFlow.getChildren().addAll(bSettings);
		
		
	}
	
	public void setSideFlow(ScrollPane pane){
		sideFlow = pane;
	}
	
}
