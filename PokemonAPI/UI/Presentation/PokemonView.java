package Presentation;

import contracts.Pokemon.Pokemon;
import contracts.Pokemon.PokemonType;
import controllers.PokemonController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class PokemonView extends GridPane{
	Pokemon poke;
	VBox vbox;
	
	HBox nameBox; 
	HBox typesBox;
	
	public PokemonView(Pokemon poke) {
		this.poke = poke;
		initialize();
	}
	
	public void initialize() {
		setPadding(new Insets(10));
		setAlignment(Pos.TOP_LEFT);
		setHgap(5);
		setVgap(1);
		
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setHalignment(HPos.RIGHT);
		
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setHalignment(HPos.LEFT);
		
		ColumnConstraints column3 = new ColumnConstraints();
		column3.setHgrow(Priority.ALWAYS);
		
		getColumnConstraints().addAll(column1, column2, column3);		
		
		
		Label nameLabel = new Label("Name: ");
		Label name = new Label(poke.getName());
		
		Label typeLabel = new Label("Type(s): ");
		String types = "";
		for(PokemonType type: poke.getTypes()) {
			types += type.getType().getName() + " ";
		}
		Label lTypes = new Label(types);
		
		addColumn(0, nameLabel, typeLabel);
		addColumn(1, name, lTypes);
		
		ImageView selectedImage = new ImageView();
		try 
		{
			Image image = new Image(PokemonController.getPokemonFrontDefaultPicture(poke));
			
			selectedImage.setImage(image);
			add(selectedImage, 3, 0, 2, 3);
		} 
		catch(Exception e) {
			
		}
		
		
	}
}
