package Presentation;

import src.PokemonSettings;
import src.RegionEnum;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SettingsView {
	private Scene settingsScene;
	private Stage settingsStage = new Stage();
	private BorderPane borderPane = new BorderPane();
	private PokemonSettings settings;
	private HBox hBottom = new HBox();
	private VBox vCenter = new VBox();
	
	public void start() {
		settingsScene = new Scene(borderPane, 300, 500);
		settingsStage.setScene(settingsScene);
		
		settings = PokemonSettings.getInstance();
		
		ComboBox<RegionEnum> cbRegions = new ComboBox<RegionEnum>();
		cbRegions.getItems().setAll(RegionEnum.values());
		cbRegions.getSelectionModel().select(settings.getPokemonRegion());
		HBox hRegions = new HBox();
		hRegions.setAlignment(Pos.CENTER);
		hRegions.getChildren().addAll(new Label("Region: "), cbRegions);
		vCenter.getChildren().add(hRegions);
		vCenter.setAlignment(Pos.CENTER);
		
		Button bSave = new Button("Save");
		bSave.setOnMouseClicked((ae) -> {
			settings.setPokemonRegion(cbRegions.getValue());
			settingsStage.close();
		}
		);
		
		Button bCancel = new Button("Cancel");
		bCancel.setOnMouseClicked(ae -> settingsStage.close());
		
		borderPane.setPadding(new Insets(5,5,5,5));
		
		hBottom.setSpacing(5.0);
		hBottom.setAlignment(Pos.CENTER_RIGHT);
		hBottom.getChildren().addAll(bSave, bCancel);
		BorderPane.setAlignment(vCenter, Pos.CENTER);
		BorderPane.setAlignment(hBottom, Pos.CENTER_RIGHT);
		borderPane.setCenter(vCenter);
		borderPane.setBottom(hBottom);
		
		settingsStage.show();
	}
}
