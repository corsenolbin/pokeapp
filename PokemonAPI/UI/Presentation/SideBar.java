package Presentation;

import java.util.List;

import contracts.Pokemon.Pokemon;
import controllers.PokemonController;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SideBar extends ScrollPane{
	
	public VBox vbox;
	public BorderPane parent;
	Button selected;
	
	private static final String HOVERED_BUTTON = "-fx-background-color: lightskyblue;";
	private static final String SELECTED_BUTTON = "-fx-background-color: lightskyblue;";
	private static final String STANDARD_BUTTON = "-fx-border-width: 0;"
			+ "-fx-border-radius: 0;"
			+ "-fx-text-alignment: left;"
			+ "-fx-background-radius: 0;"
			+ "-fx-background-color: white;";
	
	public SideBar(BorderPane rootNode) {
		parent = rootNode;
		setStyle("-fx-background: white;"
				+ "-fx-background-color: white;"
				+ "-fx-border-color: transparent;"
				+ "-fx-focus-color: transparent;"
				+ "-fx-faint-focus-color: transparent;"
				+ "-fx-margin: 0;"
				+ "-fx-border-width: 0;");
		
		setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		setHbarPolicy(ScrollBarPolicy.NEVER);

		vbox = new VBox();
		vbox.setPrefWidth(125);
		vbox.setMaxWidth(Double.MAX_VALUE);
		// just load pokemon for now
		List<String> names	= PokemonController.getAllPokemonNamesFromFiles();
		
		for(String name: names) {
			vbox.getChildren().add(createButton(name));
		}
		
		selected = (Button) vbox.getChildren().get(0);
		Pokemon poke = PokemonController.getPokemonByName(selected.getText());
		PokemonView pokeView = new PokemonView(poke);
		parent.setCenter(pokeView);
		selected.setStyle(SELECTED_BUTTON);
	//	selected = (Button) selectedBox.getChildren().get(0);
		
		setContent(vbox);
	}
	
	private Button createButton(String name) {
		HBox buttContainer = new HBox();
		buttContainer.setAlignment(Pos.BASELINE_LEFT);
		Button butt = new Button(name);
		butt.setAlignment(Pos.CENTER_LEFT);
		
		setAllStyles(butt);
		
		butt.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(butt, Priority.ALWAYS);
		
		butt.setOnMouseClicked((e) -> {
			
			selected.setStyle(STANDARD_BUTTON);	
			selected = butt;
			selected.setStyle(SELECTED_BUTTON);
			Pokemon poke = PokemonController.getPokemonByName(name);
			PokemonView pokeView = new PokemonView(poke);
			parent.setCenter(pokeView);
			
			Stage statStage = new Stage();
	        
	        StatGrid statGrid = new StatGrid(poke);
	        Scene statScene = new Scene(statGrid, 300, 300);
	        statStage.setScene(statScene);
	        statStage.show();
		});
		
		buttContainer.getChildren().add(butt);
		
        
        
		return butt;
	}

	
	private void setAllStyles(Node n) {
		n.setStyle(STANDARD_BUTTON);
		n.setOnMouseEntered(e -> n.setStyle(HOVERED_BUTTON));
		n.setOnMouseExited(e -> {
			if(!n.equals(selected))
				n.setStyle(STANDARD_BUTTON);
		});
	}
}
