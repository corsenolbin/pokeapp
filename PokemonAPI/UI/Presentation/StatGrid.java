package Presentation;

import src.StatEnum;
import contracts.Pokemon.Pokemon;
import javafx.scene.Node;
import javafx.scene.control.Label;

public class StatGrid extends InfoGrid{
    
    public StatGrid(Pokemon poke) 
    {
        super();
        
        Node[] lStats = new Node[6];
        
        int i = 0;
        for(StatEnum stat: StatEnum.values())
        {
            lStats[i++] = new Label(stat.name);
        }
        addColumn(0, lStats);
        
        Node[] lNums = new Node[6];
        for(i = 0; i < 6; i++) 
        {
            int baseStat = poke.getStats().get(5-i).getBaseStat();
            lNums[i] = new Label(Integer.toString(baseStat));
        }
        addColumn(1, lNums);
    }
}