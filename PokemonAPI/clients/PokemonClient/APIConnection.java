package PokemonClient;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class APIConnection<T> {

	private static final String USER_AGENT = "Mozilla/5.0";
	
	
	// HTTP GET request
	public static <T> T sendGet(String url, Class<T> type) {
		
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			// optional default is GET
			con.setRequestMethod("GET");
	
			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
	
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
		
			BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			GsonBuilder builder = new GsonBuilder();
			
			Gson gson = builder.setPrettyPrinting().create();
			String responseStr = response.toString();

			return gson.fromJson(responseStr, type);

			
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		

		return null;
	}
	
	public static BufferedImage sendGetForImage(String url) {
		BufferedImage image = null;
		try {
			URL obj = new URL(url);
			image = ImageIO.read(obj);
	
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return image;
		
	}
}