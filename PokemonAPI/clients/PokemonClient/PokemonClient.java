package PokemonClient;

import java.awt.image.BufferedImage;

import contracts.Pokemon.*;

public class PokemonClient {
	
	static String URLbase = "https://pokeapi.co/api/v2/";
	public static Pokemon GetPokemonById(int id) {
		String url =  URLbase + "pokemon/" + id + "/";	
		System.out.println("loading... " + url);
		return APIConnection.sendGet(url, Pokemon.class);	
	}
	
	public static Pokemon GetPokemonByName(String name) {
		String url = URLbase + "pokemon/" + name + "/";	
		return APIConnection.sendGet(url, Pokemon.class);
	}
	
	public static Ability GetAbilityById(int id) {
		String url = URLbase + "ability/" + id + "/";
		return APIConnection.sendGet(url, Ability.class);
	}
	
	public static Ability GetAbilityByName(String name) {
		String url = URLbase + "ability/" + name + "/";
		return APIConnection.sendGet(url, Ability.class);
	}
	
	public static BufferedImage GetImageByURL(String url) {
		return APIConnection.sendGetForImage(url);
	}
	
    /**
     * Experimental: Try not to use this, instead add method to PokemonClient.java
     * and interface that through a controller.
     * @param url
     * @param genClass
     * @return T
     */
	public static <T> T GetObjectByURL(String url, Class<T> genClass) {
		return APIConnection.sendGet(url, genClass);
	}
}
