package PokemonClient;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import contracts.Pokemon.Pokemon;

class PokemonClientTest {

	@Test
	void testGetPokemonById() {
		Pokemon pikachu = PokemonClient.GetPokemonById(25);
		
		assertEquals(pikachu.getName(), "pikachu");
	}

	@Test
	void testGetPokemonByName() {
		Pokemon squirtle = PokemonClient.GetPokemonByName("squirtle");
		
		assertEquals(squirtle.getName(), "squirtle");
	}

}
