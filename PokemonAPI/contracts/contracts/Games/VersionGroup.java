package contracts.Games;

import java.io.Serializable;
import java.util.ArrayList;

import contracts.Locations.Region;
import contracts.Moves.MoveLearnMethod;

public class VersionGroup implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private int order;
	private Generation generation;
	private ArrayList<MoveLearnMethod> move_learn_methods;
	private ArrayList<Pokedex> pokedexes;
	private ArrayList<Region> regions;
	private ArrayList<Version> versions;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public Generation getGeneration() {
		return generation;
	}
	public void setGeneration(Generation generation) {
		this.generation = generation;
	}
	public ArrayList<MoveLearnMethod> getMove_learn_methods() {
		return move_learn_methods;
	}
	public void setMove_learn_methods(ArrayList<MoveLearnMethod> move_learn_methods) {
		this.move_learn_methods = move_learn_methods;
	}
	public ArrayList<Pokedex> getPokedexes() {
		return pokedexes;
	}
	public void setPokedexes(ArrayList<Pokedex> pokedexes) {
		this.pokedexes = pokedexes;
	}
	public ArrayList<Region> getRegions() {
		return regions;
	}
	public void setRegions(ArrayList<Region> regions) {
		this.regions = regions;
	}
	public ArrayList<Version> getVersions() {
		return versions;
	}
	public void setVersions(ArrayList<Version> versions) {
		this.versions = versions;
	}
	

}
