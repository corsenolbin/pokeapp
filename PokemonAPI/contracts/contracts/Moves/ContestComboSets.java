package contracts.Moves;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class ContestComboSets {
	@SerializedName("use_before")
	@Expose
	private List<NamedAPIResource> useBefore;
	@SerializedName("use_after")
	@Expose
	private List<NamedAPIResource> useAfter;
	
	public List<NamedAPIResource> getUseBefore() {
		return useBefore;
	}
	public void setUseBefore(List<NamedAPIResource> useBefore) {
		this.useBefore = useBefore;
	}
	public List<NamedAPIResource> getUseAfter() {
		return useAfter;
	}
	public void setUseAfter(List<NamedAPIResource> useAfter) {
		this.useAfter = useAfter;
	}
}
