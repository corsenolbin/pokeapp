package contracts.Moves;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Pokemon.AbilityEffectChange;
import contracts.Pokemon.Name;
import contracts.Utility.NamedAPIResource;

public class Move{
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("accuracy")
	@Expose
	private int accuracy;
	@SerializedName("effect_chance")
	@Expose
	private int effectChance;
	@SerializedName("pp")
	@Expose
	private int pp;
	@SerializedName("priority")
	@Expose
	private int priority;
	@SerializedName("power")
	@Expose
	private int power;
	@SerializedName("contest_combos")
	@Expose
	private ContestComboSets contestCombos;
	@SerializedName("contest_type")
	@Expose
	private NamedAPIResource contestType;
	@SerializedName("contest_effect")
	@Expose
	private APIResource contestEffect;
	@SerializedName("damage_class")
	@Expose
	private NamedAPIResource damageClass;
	@SerializedName("effect_entries")
	@Expose
	private List<VerboseEffect> effectEntries = null;
	@SerializedName("effect_changes")
	@Expose
	private List<AbilityEffectChange> effectChanges = null;
	@SerializedName("generation")
	@Expose
	private NamedAPIResource generation;
	@SerializedName("meta")
	@Expose
	private MoveMetaData meta;
	@SerializedName("names")
	@Expose
	private List<Name> names = null;
	@SerializedName("past_values")
	@Expose
	private List<PastMoveStatValues> pastValues = null;
	@SerializedName("stat_changes")
	@Expose
	private List<MoveStatChange> statChanges = null;
	@SerializedName("super_contest_effect")
	@Expose
	private APIResource superContestEffect;
	@SerializedName("target")
	@Expose
	private NamedAPIResource target;
	@SerializedName("type")
	@Expose
	private NamedAPIResource type;
	@SerializedName("flavor_text_entries")
	@Expose
	private List<MoveFlavorText> flavorTextEntries = null;

	public int getId() {
	return id;
	}

	public void setId(int id) {
	this.id = id;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public int getAccuracy() {
	return accuracy;
	}

	public void setAccuracy(int accuracy) {
	this.accuracy = accuracy;
	}

	public int getEffectChance() {
	return effectChance;
	}

	public void setEffectChance(int effectChance) {
	this.effectChance = effectChance;
	}

	public int getPp() {
	return pp;
	}

	public void setPp(int pp) {
	this.pp = pp;
	}

	public int getPriority() {
	return priority;
	}

	public void setPriority(int priority) {
	this.priority = priority;
	}

	public int getPower() {
	return power;
	}

	public void setPower(int power) {
	this.power = power;
	}

	public ContestComboSets getContestCombos() {
	return contestCombos;
	}

	public void setContestCombos(ContestComboSets contestCombos) {
	this.contestCombos = contestCombos;
	}

	public NamedAPIResource getContestType() {
	return contestType;
	}

	public void setContestType(NamedAPIResource contestType) {
	this.contestType = contestType;
	}

	public APIResource getContestEffect() {
	return contestEffect;
	}

	public void setContestEffect(APIResource contestEffect) {
	this.contestEffect = contestEffect;
	}

	public NamedAPIResource getDamageClass() {
	return damageClass;
	}

	public void setDamageClass(NamedAPIResource damageClass) {
	this.damageClass = damageClass;
	}

	public List<VerboseEffect> getEffectEntries() {
	return effectEntries;
	}

	public void setEffectEntries(List<VerboseEffect> effectEntries) {
	this.effectEntries = effectEntries;
	}

	public List<AbilityEffectChange> getEffectChanges() {
	return effectChanges;
	}

	public void setEffectChanges(List<AbilityEffectChange> effectChanges) {
	this.effectChanges = effectChanges;
	}

	public NamedAPIResource getGeneration() {
	return generation;
	}

	public void setGeneration(NamedAPIResource generation) {
	this.generation = generation;
	}

	public MoveMetaData getMeta() {
	return meta;
	}

	public void setMeta(MoveMetaData meta) {
	this.meta = meta;
	}

	public List<Name> getNames() {
	return names;
	}

	public void setNames(List<Name> names) {
	this.names = names;
	}

	public List<PastMoveStatValues> getPastValues() {
	return pastValues;
	}

	public void setPastValues(List<PastMoveStatValues> pastValues) {
	this.pastValues = pastValues;
	}

	public List<MoveStatChange> getStatChanges() {
	return statChanges;
	}

	public void setStatChanges(List<MoveStatChange> statChanges) {
	this.statChanges = statChanges;
	}

	public APIResource getSuperContestEffect() {
	return superContestEffect;
	}

	public void setSuperContestEffect(APIResource superContestEffect) {
	this.superContestEffect = superContestEffect;
	}

	public NamedAPIResource getTarget() {
	return target;
	}

	public void setTarget(NamedAPIResource target) {
	this.target = target;
	}

	public NamedAPIResource getType() {
	return type;
	}

	public void setType(NamedAPIResource type) {
	this.type = type;
	}

	public List<MoveFlavorText> getFlavorTextEntries() {
	return flavorTextEntries;
	}

	public void setFlavorTextEntries(List<MoveFlavorText> flavorTextEntries) {
	this.flavorTextEntries = flavorTextEntries;
	}
}
