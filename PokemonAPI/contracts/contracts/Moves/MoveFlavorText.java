package contracts.Moves;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class MoveFlavorText {
	@SerializedName("flavor_text")
	@Expose
	private String flavorText;
	private NamedAPIResource language;
	@SerializedName("version_group")
	@Expose
	private NamedAPIResource versionGroup;
	public String getFlavorText() {
		return flavorText;
	}
	public NamedAPIResource getLanguage() {
		return language;
	}
	public NamedAPIResource getVersionGroup() {
		return versionGroup;
	}
	
}
