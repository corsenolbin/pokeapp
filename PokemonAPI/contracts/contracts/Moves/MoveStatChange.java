package contracts.Moves;

import contracts.Utility.NamedAPIResource;

public class MoveStatChange {
	private int change;
	private NamedAPIResource stat;
	
	public int getChange() {
		return change;
	}
	public NamedAPIResource getStat() {
		return stat;
	}
	
	
}
