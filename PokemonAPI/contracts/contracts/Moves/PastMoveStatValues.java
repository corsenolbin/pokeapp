package contracts.Moves;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PastMoveStatValues {
	private int accuracy;
	@SerializedName("effect_chance")
	@Expose
	private int effectChance;
	private int power;
	private int pp;
	@SerializedName("effect_entries")
	@Expose
	private List<VerboseEffect> effectEntries;
	@SerializedName("type")
	@Expose
	private NamedAPIResource type;
	@SerializedName("version_group")
	@Expose
	private NamedAPIResource versionGroup;
	
	public int getAccuracy() {
		return accuracy;
	}
	public int getEffectChance() {
		return effectChance;
	}
	public int getPower() {
		return power;
	}
	public int getPp() {
		return pp;
	}
	public List<VerboseEffect> getEffectEntries() {
		return effectEntries;
	}
	public NamedAPIResource getType() {
		return type;
	}
	public NamedAPIResource getVersionGroup() {
		return versionGroup;
	}
	
	
}
