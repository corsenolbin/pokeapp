package contracts.Moves;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class VerboseEffect {
	@SerializedName("effect")
	@Expose
	private String effect;
	@SerializedName("short_effect")
	@Expose
	private String shortEffect;
	private NamedAPIResource language;
	
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public String getShortEffect() {
		return shortEffect;
	}
	public void setShortEffect(String shortEffect) {
		this.shortEffect = shortEffect;
	}
	public NamedAPIResource getLanguage() {
		return language;
	}
	public void setLanguage(NamedAPIResource language) {
		this.language = language;
	}
	
	
}
