package contracts.Pokemon;

import java.util.ArrayList;

import contracts.Games.Generation;

public class Ability{
	private int id;
	private String name;
	private boolean is_main_series;
	private Generation generation;
	private ArrayList<Name> names;
	private ArrayList<EffectEntry> effect_entries;
	private ArrayList<AbilityEffectChange> effect_changes;
	private ArrayList<AbilityFlavorText> flavor_text_entries;
	private ArrayList<AbilityPokemon> pokemon;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isIs_main_series() {
		return is_main_series;
	}
	public void setIs_main_series(boolean is_main_series) {
		this.is_main_series = is_main_series;
	}
	public Generation getGeneration() {
		return generation;
	}
	public void setGeneration(Generation generation) {
		this.generation = generation;
	}
	public ArrayList<Name> getNames() {
		return names;
	}
	public void setNames(ArrayList<Name> names) {
		this.names = names;
	}
	public ArrayList<EffectEntry> getEffect_entries() {
		return effect_entries;
	}
	public void setEffect_entries(ArrayList<EffectEntry> effect_entries) {
		this.effect_entries = effect_entries;
	}
	public ArrayList<AbilityEffectChange> getEffect_changes() {
		return effect_changes;
	}
	public void setEffect_changes(ArrayList<AbilityEffectChange> effect_changes) {
		this.effect_changes = effect_changes;
	}
	public ArrayList<AbilityFlavorText> getFlavor_text_entries() {
		return flavor_text_entries;
	}
	public void setFlavor_text_entries(ArrayList<AbilityFlavorText> flavor_text_entries) {
		this.flavor_text_entries = flavor_text_entries;
	}
	public ArrayList<AbilityPokemon> getPokemon() {
		return pokemon;
	}
	public void setPokemon(ArrayList<AbilityPokemon> pokemon) {
		this.pokemon = pokemon;
	}
	
}
