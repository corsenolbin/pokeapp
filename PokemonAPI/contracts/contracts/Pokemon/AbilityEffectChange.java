package contracts.Pokemon;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class AbilityEffectChange {
	@SerializedName("effect_entries")
	@Expose
	private ArrayList<Effect> effectEntries;
	@SerializedName("version_group")
	@Expose
	private NamedAPIResource versionGroup;
	
	public ArrayList<Effect> getEffect_entries() {
		return effectEntries;
	}
	public void setEffect_entries(ArrayList<Effect> effect_entries) {
		this.effectEntries = effect_entries;
	}
	public NamedAPIResource getVersion_group() {
		return versionGroup;
	}
	public void setVersion_group(NamedAPIResource version_group) {
		this.versionGroup = version_group;
	}
}
