package contracts.Pokemon;

import contracts.Utility.Language;

public class Effect {
	private String effect;
	private Language language;
	
	
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	
	

}
