package contracts.Pokemon;

public interface IPokemonContract {
	
	public String getName();
	public String getFileName();
	public String getDirectory();

}
