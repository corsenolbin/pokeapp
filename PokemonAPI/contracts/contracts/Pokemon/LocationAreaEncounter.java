package contracts.Pokemon;

import java.util.List;

import contracts.Utility.NamedAPIResource;
import contracts.Utility.VersionEncounterDetail;

public class LocationAreaEncounter {
	private NamedAPIResource locationArea;
	private List<VersionEncounterDetail> versionDetails;
	
	public NamedAPIResource getLocationArea() {
		return locationArea;
	}
	public void setLocationArea(NamedAPIResource locationArea) {
		this.locationArea = locationArea;
	}
	public List<VersionEncounterDetail> getVersionDetails() {
		return versionDetails;
	}
	public void setVersionDetails(List<VersionEncounterDetail> versionDetails) {
		this.versionDetails = versionDetails;
	}
}
