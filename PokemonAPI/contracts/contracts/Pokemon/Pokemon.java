package contracts.Pokemon;

import java.util.HashMap;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class Pokemon extends PokemonContract{
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("base_experience")
	@Expose
	private int baseExperience;
	@SerializedName("height")
	@Expose
	private int height;
	@SerializedName("is_default")
	@Expose
	private boolean isDefault;
	@SerializedName("order")
	@Expose
	private int order;
	@SerializedName("weight")
	@Expose
	private int weight;
	@SerializedName("abilities")
	@Expose
	private List<PokemonAbility> abilities = null;
	@SerializedName("forms")
	@Expose
	private List<NamedAPIResource> forms = null;
	@SerializedName("game_indices")
	@Expose
	private List<VersionGameIndex> gameIndices = null;
	@SerializedName("held_items")
	@Expose
	private List<PokemonHeldItem> heldItems = null;
	@SerializedName("location_area_encounters")
	@Expose
	private String locationAreaEncounters;
	@SerializedName("moves")
	@Expose
	private List<PokemonMove> moves = null;
	@SerializedName("sprites")
	@Expose
	private PokemonSprites sprites;
	@SerializedName("species")
	@Expose
	private NamedAPIResource species;
	@SerializedName("stats")
	@Expose
	private List<PokemonStat> stats = null;
	@SerializedName("types")
	@Expose
	private List<PokemonType> types = null;
	
	
	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBaseExperience() {
		return baseExperience;
	}

	public void setBaseExperience(int baseExperience) {
		this.baseExperience = baseExperience;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isIsDefault() {
		return isDefault;
	}

	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public List<PokemonAbility> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<PokemonAbility> abilities) {
		this.abilities = abilities;
	}

	public List<NamedAPIResource> getForms() {
		return forms;
	}

	public void setForms(List<NamedAPIResource> forms) {
		this.forms = forms;
	}

	public List<VersionGameIndex> getGameIndices() {
		return gameIndices;
	}

	public void setGameIndices(List<VersionGameIndex> gameIndices) {
		this.gameIndices = gameIndices;
	}

	public List<PokemonHeldItem> getHeldItems() {
		return heldItems;
	}

	public void setHeldItems(List<PokemonHeldItem> heldItems) {
		this.heldItems = heldItems;
	}

	public String getLocationAreaEncounters() {
		return locationAreaEncounters;
	}

	public void setLocationAreaEncounters(String locationAreaEncounters) {
		this.locationAreaEncounters = locationAreaEncounters;
	}

	public List<PokemonMove> getMoves() {
		return moves;
	}

	public void setMoves(List<PokemonMove> moves) {
		this.moves = moves;
	}

	public NamedAPIResource getSpecies() {
		return species;
	}

	public void setSpecies(NamedAPIResource species) {
		this.species = species;
	}

	public HashMap<String, String> getSprites() {
		return sprites.getMap();
	}

	public void setSprites(PokemonSprites sprites) {
		this.sprites = sprites;
	}

	public List<PokemonStat> getStats() {
		return stats;
	}

	public void setStats(List<PokemonStat> stats) {
		this.stats = stats;
	}

	public List<PokemonType> getTypes() {
		return types;
	}

	public void setTypes(List<PokemonType> types) {
		this.types = types;
	}

	public String getImageFileName(String num) {
		
		return "\\";
	}	
}