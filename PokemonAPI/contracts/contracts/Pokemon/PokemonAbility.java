package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonAbility {
	
	@SerializedName("is_hidden")
	@Expose
	private boolean isHidden;
	@SerializedName("slot")
	@Expose
	private int slot;
	@SerializedName("ability")
	@Expose
	private NamedAPIResource ability;
	
	public boolean isIsHidden() {
	return isHidden;
	}
	
	public void setIsHidden(boolean isHidden) {
	this.isHidden = isHidden;
	}
	
	public int getSlot() {
	return slot;
	}
	
	public void setSlot(int slot) {
	this.slot = slot;
	}
	
	public NamedAPIResource getAbility() {
	return ability;
	}
	
	public void setAbility(NamedAPIResource ability) {
	this.ability = ability;
	}

}
