package contracts.Pokemon;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonForm {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("order")
	@Expose
	private int order;
	@SerializedName("form_order")
	@Expose
	private int formOrder;
	@SerializedName("is_default")
	@Expose
	private boolean isDefault;
	@SerializedName("is_battle_only")
	@Expose
	private boolean isBattleOnly;
	@SerializedName("is_mega")
	@Expose
	private boolean isMega;
	@SerializedName("form_name")
	@Expose
	private String formName;
	@SerializedName("pokemon")
	@Expose
	private NamedAPIResource pokemon;
	@SerializedName("sprites")
	@Expose
	private PokemonFormSprites sprites;
	@SerializedName("version_group")
	@Expose
	private NamedAPIResource versionGroup;
	@SerializedName("names")
	@Expose
	private List<Name> names;
	@SerializedName("form_names")
	@Expose
	private List<Name> formNames;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getFormOrder() {
		return formOrder;
	}

	public void setFormOrder(int formOrder) {
		this.formOrder = formOrder;
	}

	public boolean isIsDefault() {
		return isDefault;
	}

	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public boolean isIsBattleOnly() {
		return isBattleOnly;
	}

	public void setIsBattleOnly(boolean isBattleOnly) {
		this.isBattleOnly = isBattleOnly;
	}

	public boolean isIsMega() {
		return isMega;
	}

	public void setIsMega(boolean isMega) {
		this.isMega = isMega;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public NamedAPIResource getPokemon() {
		return pokemon;
	}

	public void setPokemon(NamedAPIResource pokemon) {
		this.pokemon = pokemon;
	}

	public PokemonFormSprites getSprites() {
		return sprites;
	}

	public void setSprites(PokemonFormSprites sprites) {
		this.sprites = sprites;
	}

	public NamedAPIResource getVersionGroup() {
		return versionGroup;
	}

	public void setVersionGroup(NamedAPIResource versionGroup) {
		this.versionGroup = versionGroup;
	}

	public List<Name> getNames() {
		return names;
	}

	public void setNames(List<Name> names) {
		this.names = names;
	}

	public List<Name> getFormNames() {
		return formNames;
	}

	public void setFormNames(List<Name> formNames) {
		this.formNames = formNames;
	}

}
