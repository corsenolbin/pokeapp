package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonHeldItemVersion {
	@SerializedName("version")
	@Expose
	private NamedAPIResource version;
	@SerializedName("rarity")
	@Expose
	private int rarity;
	
	public NamedAPIResource getVersion() {
		return version;
	}
	public void setVersion(NamedAPIResource version) {
		this.version = version;
	}
	public int getRarity() {
		return rarity;
	}
	public void setRarity(int rarity) {
		this.rarity = rarity;
	}	
}
