package contracts.Pokemon;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonMove{
	@SerializedName("move")
	@Expose
	private NamedAPIResource move;
	@SerializedName("version_group_details")
	@Expose
	private List<PokemonMoveVersion> versionGroupDetails;
	
	public NamedAPIResource getMove() {
		return move;
	}
	public void setMove(NamedAPIResource move) {
		this.move = move;
	}
	public List<PokemonMoveVersion> getVersion_group_details() {
		return versionGroupDetails;
	}
	public void setVersion_group_details(ArrayList<PokemonMoveVersion> version_group_details) {
		this.versionGroupDetails = version_group_details;
	}

}
