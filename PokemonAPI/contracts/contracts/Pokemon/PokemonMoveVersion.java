package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonMoveVersion{
	@SerializedName("move_learn_method")
	@Expose
	private NamedAPIResource moveLearnMethod;
	@SerializedName("version_group")
	@Expose
	private NamedAPIResource versionGroup;
	@SerializedName("level_learned_at")
	@Expose
	private int levelLearnedAt;
	
	public NamedAPIResource getMoveLearnMethod() {
		return moveLearnMethod;
	}
	public void setMoveLearnMethod(NamedAPIResource moveLearnMethod) {
		this.moveLearnMethod = moveLearnMethod;
	}
	public NamedAPIResource getVersionGroup() {
		return versionGroup;
	}
	public void setVersionGroup(NamedAPIResource versionGroup) {
		this.versionGroup = versionGroup;
	}
	public int getLevelLearnedAt() {
		return levelLearnedAt;
	}
	public void setLevelLearnedAt(int levelLearnedAt) {
		this.levelLearnedAt = levelLearnedAt;
	}
	
	
}
