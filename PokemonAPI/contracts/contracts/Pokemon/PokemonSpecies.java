package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PokemonSpecies{
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("order")
	@Expose
	private int order;
	@SerializedName("gender_rate")
	@Expose
	private int genderRate;
	@SerializedName("capture_rate")
	@Expose
	private int captureRate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public int getGenderRate() {
		return genderRate;
	}
	public void setGenderRate(int genderRate) {
		this.genderRate = genderRate;
	}
	public int getCaptureRate() {
		return captureRate;
	}
	public void setCaptureRate(int captureRate) {
		this.captureRate = captureRate;
	}
	
	
}
