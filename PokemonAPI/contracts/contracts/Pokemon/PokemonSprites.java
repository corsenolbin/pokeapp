package contracts.Pokemon;

import java.util.HashMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PokemonSprites {
	@SerializedName("back_female")
	@Expose
	private String backFemale;
	@SerializedName("back_shiny_female")
	@Expose
	private String backShinyFemale;
	@SerializedName("back_default")
	@Expose
	private String backDefault;
	@SerializedName("front_female")
	@Expose
	private String frontFemale;
	@SerializedName("front_shiny_female")
	@Expose
	private String frontShinyFemale;
	@SerializedName("back_shiny")
	@Expose
	private String backShiny;
	@SerializedName("front_default")
	@Expose
	private String frontDefault;
	@SerializedName("front_shiny")
	@Expose
	private String frontShiny;
	
	public HashMap<String, String> getMap() {
		HashMap<String, String> map = new HashMap<String,String>();
		
		map.put("backFemale", backFemale);
		map.put("backShinyFemale", backShinyFemale);
		map.put("backDefault", backDefault);
		map.put("frontFemale", frontFemale);
		map.put("frontShinyFemale", frontShinyFemale);
		map.put("backShiny", backShiny);
		map.put("frontDefault", frontDefault);
		map.put("frontShiny", frontShiny);
		
		return map;		
	}
	
}
