package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonStat {
	@SerializedName("stat")
	@Expose
	private NamedAPIResource stat;
	@SerializedName("effort")
	@Expose
	private int effort;
	@SerializedName("base_stat")
	@Expose
	private int baseStat;
	
	public NamedAPIResource getStat() {
		return stat;
	}
	public void setStat(NamedAPIResource stat) {
		this.stat = stat;
	}
	public int getEffort() {
		return effort;
	}
	public void setEffort(int effort) {
		this.effort = effort;
	}
	public int getBaseStat() {
		return baseStat;
	}
	public void setBaseStat(int baseStat) {
		this.baseStat = baseStat;
	}
	
	

}
