package contracts.Pokemon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.NamedAPIResource;

public class PokemonType {
	@SerializedName("slot")
	@Expose
	private int slot;
	@SerializedName("type")
	@Expose
	private NamedAPIResource type;

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public NamedAPIResource getType() {
		return type;
	}

	public void setType(NamedAPIResource type) {
		this.type = type;
	}
}
