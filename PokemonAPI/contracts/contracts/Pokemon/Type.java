package contracts.Pokemon;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import contracts.Utility.GenerationGameIndex;

public class Type {
	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("damage_relations")
	@Expose
	private TypeRelations damageRelations;
	@SerializedName("game_indices")
	@Expose
	private List<GenerationGameIndex> gameIndices;
	
}
