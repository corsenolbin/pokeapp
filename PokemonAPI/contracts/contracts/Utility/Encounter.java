package contracts.Utility;

import java.util.ArrayList;

import contracts.Encounters.EncounterConditionValue;
import contracts.Encounters.EncounterMethod;

public class Encounter {
	private int min_level;
	private int max_level;
	private ArrayList<EncounterConditionValue> condition_values;
	private int chance;
	private EncounterMethod method;
	
	public int getMin_level() {
		return min_level;
	}
	public void setMin_level(int min_level) {
		this.min_level = min_level;
	}
	public int getMax_level() {
		return max_level;
	}
	public void setMax_level(int max_level) {
		this.max_level = max_level;
	}
	public ArrayList<EncounterConditionValue> getCondition_values() {
		return condition_values;
	}
	public void setCondition_values(ArrayList<EncounterConditionValue> condition_values) {
		this.condition_values = condition_values;
	}
	public int getChance() {
		return chance;
	}
	public void setChance(int chance) {
		this.chance = chance;
	}
	public EncounterMethod getMethod() {
		return method;
	}
	public void setMethod(EncounterMethod method) {
		this.method = method;
	}

}
