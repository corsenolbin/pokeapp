package contracts.Utility;

public class GenerationGameIndex {
	private int gameIndex;
	private NamedAPIResource generation;
	
	
	public NamedAPIResource getGeneration() {
		return generation;
	}
	public void setGeneration(NamedAPIResource generation) {
		this.generation = generation;
	}
	public int getGameIndex() {
		return gameIndex;
	}
	public void setGameIndex(int gameIndex) {
		this.gameIndex = gameIndex;
	}
}
