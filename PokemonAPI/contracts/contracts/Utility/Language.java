package contracts.Utility;

import java.util.ArrayList;
import contracts.Pokemon.Name;

public class Language {
	private int id;
	private String name;
	private boolean official;
	private String iso639;
	private String iso3166;
	private ArrayList<Name> names;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOfficial() {
		return official;
	}
	public void setOfficial(boolean official) {
		this.official = official;
	}
	public String getIso639() {
		return iso639;
	}
	public void setIso639(String iso639) {
		this.iso639 = iso639;
	}
	public String getIso3166() {
		return iso3166;
	}
	public void setIso3166(String iso3166) {
		this.iso3166 = iso3166;
	}
	public ArrayList<Name> getNames() {
		return names;
	}
	public void setNames(ArrayList<Name> names) {
		this.names = names;
	}

}
