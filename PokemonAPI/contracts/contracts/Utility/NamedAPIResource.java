package contracts.Utility;

import controllers.PokemonController;

public class NamedAPIResource {
	
	private String name;
	
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public <T> T getObjectFromURL(Class<T> resourceClass) {

		return PokemonController.getObjectfromURL(url, resourceClass);

	}
}
