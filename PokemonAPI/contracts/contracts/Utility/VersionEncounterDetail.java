package contracts.Utility;


import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionEncounterDetail {
	@SerializedName("version")
	@Expose
	private NamedAPIResource version;
	@SerializedName("max_chance")
	@Expose
	private int maxChance;
	@SerializedName("encounter_details")
	@Expose
	private List<Encounter> encounterDetails;
	
	public NamedAPIResource getVersion() {
		return version;
	}
	public void setVersion(NamedAPIResource version) {
		this.version = version;
	}
	public int getMax_chance() {
		return maxChance;
	}
	public void setMax_chance(int max_chance) {
		this.maxChance = max_chance;
	}
	public List<Encounter> getEncounterDetails() {
		return encounterDetails;
	}
	public void setEncounter_details(List<Encounter> encounterDetails) {
		this.encounterDetails = encounterDetails;
	}

}
