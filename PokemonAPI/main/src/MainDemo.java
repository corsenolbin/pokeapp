package src;

import Presentation.MasterView;
import controllers.PokemonController;
import javafx.application.Application;

public class MainDemo {

	public static void main(String[] args) {

        int from = 200;
        int to = 250;
        for (int i = from; i < to; i++) {
            PokemonController.getPokemonById(i);
        }
		
		// Load settings from file
		PokemonSettings.getInstance();
		
		// Run the UI
		Application.launch(MasterView.class, args);
		
	}
}
