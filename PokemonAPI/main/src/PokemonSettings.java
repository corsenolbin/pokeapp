package src;

import java.util.HashMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import store.PokemonFileService;

public class PokemonSettings {
	
	private static PokemonSettings instance;
	
	@SerializedName("pokemon_region")
	@Expose
	private RegionEnum pokemonRegion = RegionEnum.KANTO;
	
    @SerializedName("pokemon_saved")
    @Expose
    private HashMap<String, Integer> pokemonSaved = new HashMap<String, Integer>();

	private PokemonSettings() {
		
	}
	
	public static synchronized PokemonSettings getInstance() {
		if(instance == null) {
			instance = PokemonFileService.LoadJSONSettings();
			
			if(instance == null) {
				instance = new PokemonSettings();
			}
		}
		
		return instance;
	}
	
	
	public RegionEnum getPokemonRegion() {
		return pokemonRegion;
	}

	public void setPokemonRegion(RegionEnum pokemonRegion) {
		this.pokemonRegion = pokemonRegion;
	}
	
    public HashMap<String, Integer> getPokemonSaved() {
        return pokemonSaved;
    }
    
    public boolean pokeHashContainsValue(int id) {
        return pokemonSaved.containsValue(id);
    }
    
    public void addToPokemonSaved(String key, Integer id) {
        if(!pokemonSaved.containsKey(key))
            pokemonSaved.put(key, id);
    }
}
