package src;

public enum RegionEnum {
	KANTO ("Kanto"), 
	JOHTO ("Johto"), 
	HOENN ("Hoenn"), 
	SINNOH ("Sinnoh"), 
	UNOVA ("Unova"), 
	KALOS ("Kalos"), 
	ALOLA ("Alola");
	
	private final String regionVal;
	
	RegionEnum(String regionVal){
		this.regionVal = regionVal;
	}

	public String getRegionVal() {
		return regionVal;
	}
}
