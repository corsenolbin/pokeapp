package src;

public enum StatEnum {
    HP ("HP"),
    ATTACK ("Attack"),
    DEFENSE ("Defense"),
    SPATTACK ("Sp. Attack"),
    SPDEFENSE ("Sp. Defense"),
    SPEED ("Speed");
    
    public String name;
    
    StatEnum(String name){
        this.name = name;
    }
}
