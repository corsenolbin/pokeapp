/**
 * 
 */
/**
 * @author Corbin
 *
 */
module PokemonAPI {
	exports contracts.Moves;
	exports contracts.Utility;
	exports contracts.Games;
	exports contracts.Locations;
	exports PokemonClient;
	exports contracts.Encounters;
	exports store;
	exports contracts.Pokemon;
	exports src;
	exports Presentation;

	requires gson;
	requires org.junit.jupiter.api;
	requires org.junit.platform.commons;
	requires java.sql;
	requires transitive java.desktop;
	requires transitive javafx.graphics;
	requires transitive javafx.controls;
	
}