package store;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import src.PokemonSettings;
import PokemonClient.PokemonClient;
import contracts.Pokemon.Pokemon;
import contracts.Pokemon.PokemonContract;

public class PokemonFileService {
	
	public static void SaveObject(PokemonContract contract) {
		try {
			File newFile = createFile(contract);
			
			if(newFile.exists()) {
				System.out.println("File already exists.");
				return;
			}
			
			FileOutputStream fout = new FileOutputStream(newFile);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			
			oos.writeObject(contract);
			
			oos.close();
			System.out.println("file written");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void SaveJSONObject(PokemonContract contract) {
		Gson gson = new GsonBuilder().serializeNulls()
				  .setPrettyPrinting()
				  .create();
		
		String fileName = createFileName(contract.getName());
		
        File file = new File(fileName);
        
        // don't save file if it already exists
        if(file.exists()) 
        {
            return;
        }
        
		try {
			System.out.println("Saving... " + fileName);
			FileWriter writer = new FileWriter(fileName);
			gson.toJson(contract, writer);
			writer.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void SaveJSONSettings(PokemonSettings instance) {
		Gson gson = new GsonBuilder().serializeNulls()
				.setPrettyPrinting()
				.excludeFieldsWithoutExposeAnnotation()
				.create();
		
		try {
			FileWriter writer = new FileWriter("settings.json");
			gson.toJson(instance, writer);
			writer.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static PokemonSettings LoadJSONSettings() {
		File file = new File("settings.json");
		
		try {
			Gson gson = new Gson();
			System.out.println("Loading from file... " + file.toString());
			FileReader fileReader = new FileReader(file);
			BufferedReader br = new BufferedReader(fileReader);
			
			PokemonSettings response = gson.fromJson(br, PokemonSettings.class);
			
			br.close();
			fileReader.close();
			return response;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static Pokemon LoadJSONObject(String name) {
		
		File file = fileSearch(name);
				
		if(file == null) {
			System.out.println("File does not exist.");
			return null;
		}
		
		try {
			Gson gson = new Gson();
			System.out.println("Loading from file... " + file.toString());
			FileReader fileReader = new FileReader(file);
			BufferedReader br = new BufferedReader(fileReader);
			
			Pokemon response = gson.fromJson(br, Pokemon.class);
			
			br.close();
			fileReader.close();
			return response;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
				
	}
	
	
	private static File createFile(PokemonContract contract) {
		String currentDir = System.getProperty("user.dir");
		
		System.out.println(currentDir);
		
		File f = new File(currentDir);
		String parent = f.getParent();
		System.out.println(parent);
		
		File directory = new File(parent + contract.getDirectory());
		directory.mkdirs();
		
		return new File(parent + contract.getFileName());
		
	}
	
	private static File fileSearch(String name) {
		String fileName = createFileName(name);
		
		File fileToCheck = new File(fileName);
		
		if(fileToCheck.exists()) return fileToCheck;
		
		return null;		
	}
	
	public static Pokemon PokeLoad(Scanner scan) {
		Pokemon response;
		
		String input = scan.nextLine();

		try {
			response = PokemonClient.GetPokemonByName(input);
			
		} catch (Exception e) {
			response = new Pokemon();
		}
		
		return response;	
	}
	
	public static File createImageFile(Pokemon contract, String key) {
		
		File directory = new File(getDataPath() + "\\pics\\");
		directory.mkdirs();
		
		return new File(directory + "\\" + contract.getName() + key + ".png");
		
	}
	
	public static void WriteImage(BufferedImage image, File file) {
		if(image == null) {
			return;
		}
		
		try {
			ImageIO.write(image, "png", file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String createFileName(String name) {
		String currentDir = System.getProperty("user.dir");
		
		System.out.println(currentDir);
		
		File f = new File(currentDir);
		String parent = f.getParent();

		File directory = new File(parent + "\\data\\pokemon\\");
		
		if(!directory.exists())
			directory.mkdirs();
		
		return parent + "\\data\\pokemon\\" + name + ".json";
	}
	
	public static List<String> getAllPokemonNames() {
		List<String> names = new ArrayList<String>();

		File directory = new File(getDataPath() + "pokemon\\");
		
		File[] files = directory.listFiles();
		
		for(File file: files) {
			if(file.isFile()) {
				// System.out.println(file.getName());
				names.add(file.getName().replaceAll(".json", ""));
			}
		}
		
		return names;
	}
	
	public static String getDataPath() {
		String currentDir = System.getProperty("user.dir");
		
		System.out.println(currentDir);
		
		File f = new File(currentDir);
		String parent = f.getParent();
		return parent + "\\data\\";
	}
	
	public static FileInputStream getPictureStream(String name, String imageType) {
		try {
			return new FileInputStream(getDataPath() + "\\pics\\" + name + imageType + ".png");
		} catch (FileNotFoundException e) {
			System.out.println("File does not exist for " + name + " on disk...");
		}
		return null;
	}
	

}
